import { StyleSheet } from 'react-native';

export const seasonality = {STRONG: "green", MEDIUM: "yellow", WEAK: "red"};

const images = {
  calendarRed: {
    uri: require('../img/calendar_red.png')
  },
  calendarYellow: {
    uri: require('../img/calendar_yellow.png')
  },
  calendarGreen: {
      uri: require('../img/calendar_green.png')
    }
}

export const defineSeason = (fruit, month) => {
    if(fruit.seasonStrongMonths.includes(month))
        return { seasonColor: seasonality.STRONG, seasonImg: images.calendarGreen.uri };
    else if(fruit.seasonMediumMonths !== undefined && fruit.seasonMediumMonths.includes(month))
        return { seasonColor: seasonality.MEDIUM, seasonImg: images.calendarYellow.uri };
    else
        return { seasonColor: seasonality.WEAK, seasonImg: images.calendarRed.uri };
}

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
  },
  baseText: {
    fontFamily: 'Cochin',
  },
  linkList: {
    fontSize: 8,
    fontWeight: 'bold',
    color: '#04B4AE'
  },
  greaterImg: {
      width: 23,
      height: 23,
      position: 'absolute',
      right: 0,
      //marginTop: 10,
  },
  calendarImg: {
        alignSelf: 'center',
        width: '90%',
        height: '90%'
        //borderWidth: 2, borderColor: 'black'
        //right: 0,
        //marginTop: 10
    },
});

export const monthsCalendar = [ {number: 0, text: 'JAN'}, {number: 1, text: 'FEV'}, {number: 2, text: 'MAR'},
    {number: 3, text: 'ABR'}, {number: 4, text: 'MAI'}, {number: 5, text: 'JUN'}, {number: 6, text: 'JUL'},
    {number: 7, text: 'AGO'}, {number: 8, text: 'SET'}, {number: 9, text: 'OUT'}, {number: 10, text: 'NOV'},
    {number: 11, text: 'DEZ'} ];
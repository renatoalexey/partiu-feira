export default () => {
    return {
        STRONG: "STRONG",
        MEDIUM: "MEDIUM",
        WEAK: "WEAK"
    };
}
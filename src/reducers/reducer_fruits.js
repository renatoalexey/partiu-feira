export default function() {
  return [
  	{imgNameFile: "abacate", name: "Abacate Breda", seasonStrongMonths: [8, 9, 10], seasonMediumMonths: [11] },
 	{imgNameFile: "abacate_geada", name: "Abacate Geada", seasonStrongMonths: [0, 1] },
 	{imgNameFile: "abacate_fortuna", name: "Abacate Fortuna", seasonStrongMonths: [2, 3, 4, 5, 6, 7] },
 	{imgNameFile: "abacaxi_havai", name: "Abacaxi Havaí ", seasonStrongMonths: [0, 11], seasonMediumMonths: [1, 10] },
 	{imgNameFile: "abacaxi_perola", name: "Abacaxi Pérola ", seasonStrongMonths: [10, 11], seasonMediumMonths: [2, 3, 4, 9] },
 	{imgNameFile: "abiu", name: "Abiu", seasonStrongMonths: [2, 3, 7, 8], seasonMediumMonths: [0, 1, 4, 6, 9] },
 	{imgNameFile: "acerola", name: "Acerola", seasonStrongMonths: [8, 9, 10, 11], seasonMediumMonths: [0, 1, 2, 3, 4] },
 	{imgNameFile: "ameixa_estrangeira", name: "Ameixa Estrangeira", seasonStrongMonths: [1, 2, 3], seasonMediumMonths: [0, 7, 8, 9, 11] },
 	{imgNameFile: "ameixa_nacional", name: "Ameixa Nacional", seasonStrongMonths: [11], seasonMediumMonths: [0, 1, 9, 10] },
 	{imgNameFile: "amendoa", name: "Amêndoa", seasonStrongMonths: [1, 11], seasonMediumMonths: [8, 10] },
 	{imgNameFile: "atemoia", name: "Atemoia", seasonStrongMonths: [3, 4, 5, 6, 7], seasonMediumMonths: [2, 8, 9] },
 	{imgNameFile: "avela", name: "Avelã", seasonStrongMonths: [11] },
 	{imgNameFile: "banana", name: "Banana", seasonStrongMonths: [2, 7, 9, 10], seasonMediumMonths: [4, 8] },
 	{imgNameFile: "banana_maca", name: "Banana Maçã", seasonStrongMonths: [2, 3, 4], seasonMediumMonths: [0, 1, 5, 6, 7] },
 	{imgNameFile: "banana_nanica", name: "Banana Nanica", seasonStrongMonths: [2, 7, 8, 9, 10] },
 	{imgNameFile: "banana_prata", name: "Banana Prata", seasonStrongMonths: [9, 10, 11], seasonMediumMonths: [2, 4, 7, 8] },
 	{imgNameFile: "caju", name: "Caju", seasonStrongMonths: [7, 8, 9, 10], seasonMediumMonths: [0, 5, 6, 11] },
 	{imgNameFile: "caqui", name: "Caqui", seasonStrongMonths: [3, 4], seasonMediumMonths: [2, 5] },
 	{imgNameFile: "carambola", name: "Carambola", seasonStrongMonths: [0, 1, 5, 6, 7], seasonMediumMonths: [2, 3, 4, 8, 11] },
 	{imgNameFile: "castanha", name: "Castanha", seasonStrongMonths: [11], seasonMediumMonths: [10] },
 	{imgNameFile: "cereja_estrangeira", name: "Cereja Estrangeira", seasonStrongMonths: [11], seasonMediumMonths: [0] },
 	{imgNameFile: "cidra", name: "Cidra", seasonStrongMonths: [3], seasonMediumMonths: [2, 7, 8, 11] },
 	{imgNameFile: "coco_verde", name: "Côco Verde", seasonStrongMonths: [0, 1, 2, 9, 10, 11], seasonMediumMonths: [8] },
 	{imgNameFile: "damasco_estrangeiro", name: "Damasco Estrangeiro", seasonStrongMonths: [11] },
 	{imgNameFile: "figo", name: "Figo", seasonStrongMonths: [0, 1, 2, 11], seasonMediumMonths: [3] },
 	{imgNameFile: "framboesa", name: "Framboesa", seasonStrongMonths: [0, 10, 11], seasonMediumMonths: [1, 2] },
 	{imgNameFile: "fruta_conde", name: "Fruta do Conde/Pinha", seasonStrongMonths: [0, 1, 2], seasonMediumMonths: [3, 4] },
 	{imgNameFile: "goiaba", name: "Goiaba", seasonStrongMonths: [1, 2], seasonMediumMonths: [0, 3, 7, 8, 9] },
 	{imgNameFile: "graviola", name: "Graviola", seasonStrongMonths: [11], seasonMediumMonths: [2, 3, 10] },
 	{imgNameFile: "grapefruit", name: "Grapefruit", seasonStrongMonths: [6, 8, 10], seasonMediumMonths: [4, 5, 9] },
 	{imgNameFile: "jabuticaba", name: "Jabuticaba", seasonStrongMonths: [8, 9] },
 	{imgNameFile: "jaca", name: "Jaca", seasonStrongMonths: [1, 2, 3, 4, 10], seasonMediumMonths: [0, 5, 9, 11] },
 	{imgNameFile: "kiwi_nacional", name: "Kiwi Nacional", seasonStrongMonths: [3, 4, 5, 6, 7], seasonMediumMonths: [8, 9] },
 	{imgNameFile: "kiwi_estrangeiro", name: "Kiwi Estrangeiro", seasonStrongMonths: [7, 11], seasonMediumMonths: [4, 6, 8, 10] },
 	{imgNameFile: "laranja", name: "Laranja", seasonStrongMonths: [7, 8, 9], seasonMediumMonths: [0, 1, 2, 6, 10, 11] },
 	{imgNameFile: "laranja_lima", name: "Laranja Lima", seasonStrongMonths: [5, 6, 7, 8], seasonMediumMonths: [3, 4, 9] },
 	{imgNameFile: "laranja_pera", name: "Laranja Pêra", seasonStrongMonths: [0, 1, 2, 7, 8, 9, 10, 11] },
 	{imgNameFile: "lichia", name: "Lichia", seasonStrongMonths: [11], seasonMediumMonths: [10] },
 	{imgNameFile: "lima_persia", name: "Lima da Pérsia", seasonStrongMonths: [7, 9], seasonMediumMonths: [2, 3, 6, 8, 10] },
 	{imgNameFile: "limao", name: "Limão", seasonStrongMonths: [2, 11], seasonMediumMonths: [0, 1, 3] },
 	{imgNameFile: "limao_taiti", name: "Limão Taiti", seasonStrongMonths: [2, 11], seasonMediumMonths: [0, 1, 3] },
 	{imgNameFile: "maca_nacional", name: "Maçã Nacional", seasonStrongMonths: [1, 2, 3, 4, 7], seasonMediumMonths: [5, 6, 8, 9] },
 	{imgNameFile: "maca_nacional_fuji", name: "Maçã Nacional Fuji", seasonStrongMonths: [8, 9, 10, 11], seasonMediumMonths: [0, 6, 7] },
 	{imgNameFile: "maca_nacional_gala", name: "Maçã Nacional Gala", seasonStrongMonths: [1, 2, 3, 4], seasonMediumMonths: [5, 6, 7, 8] },
 	{imgNameFile: "maca_estrangeira", name: "Maçã Estrangeira", seasonStrongMonths: [10, 11], seasonMediumMonths: [0, 6, 7, 8, 9] },
 	{imgNameFile: "maca_granny_smith", name: "Maçã Granny Smith", seasonStrongMonths: [11], seasonMediumMonths: [9] },
 	{imgNameFile: "maca_red_del", name: "Maçã Red Del", seasonStrongMonths: [9, 10, 11], seasonMediumMonths: [0, 6, 7, 8] },
 	{imgNameFile: "mamao_formosa", name: "Mamão Formosa", seasonStrongMonths: [2, 3, 7], seasonMediumMonths: [4, 5, 8, 9] },
 	{imgNameFile: "mamao_havai", name: "Mamão Havaí", seasonStrongMonths: [0, 2, 9, 10], seasonMediumMonths: [1, 3, 8, 11] },
 	{imgNameFile: "manga", name: "Manga", seasonStrongMonths: [9, 10, 11], seasonMediumMonths: [0, 8] },
 	{imgNameFile: "mangostao", name: "Mangostão", seasonStrongMonths: [2, 5], seasonMediumMonths: [6] },
 	{imgNameFile: "maracuja", name: "Maracujá", seasonStrongMonths: [0, 10, 11], seasonMediumMonths: [2, 3, 7, 8, 9] },
 	{imgNameFile: "marmelo_nacional", name: "Marmelo Nacional", seasonStrongMonths: [0], seasonMediumMonths: [1, 11] },
 	{imgNameFile: "marmelo_estrangeiro", name: "Marmelo Estrangeiro", seasonStrongMonths: [5], seasonMediumMonths: [3, 4] },
 	{imgNameFile: "melancia", name: "Melancia", seasonStrongMonths: [0, 10, 11], seasonMediumMonths: [1, 2, 3, 9] },
 	{imgNameFile: "melao_amarelo", name: "Melão Amarelo", seasonStrongMonths: [10, 11], seasonMediumMonths: [0, 8, 9] },
 	{imgNameFile: "mexerica", name: "Mexerica", seasonStrongMonths: [5, 6, 7, 8], seasonMediumMonths: [4, 9] },
 	{imgNameFile: "morango", name: "Morango", seasonStrongMonths: [7], seasonMediumMonths: [5, 6, 8, 9] },
 	{imgNameFile: "nectarina_nacional", name: "Nectarina Nacional", seasonStrongMonths: [10, 11], seasonMediumMonths: [9] },
 	{imgNameFile: "nectarina_estrangeira", name: "Nectarina Estrangeira", seasonStrongMonths: [0, 2, 11], seasonMediumMonths: [1, 7] },
 	{imgNameFile: "nespera", name: "Nêspera", seasonStrongMonths: [8, 9], seasonMediumMonths: [6, 7] },
 	{imgNameFile: "nozes", name: "Nozes", seasonStrongMonths: [10, 11] },
 	{imgNameFile: "pera_nacional", name: "Pêra Nacional", seasonStrongMonths: [1, 2], seasonMediumMonths: [0, 3] },
 	{imgNameFile: "pera_estrangeira", name: "Pêra Estrangeira", seasonStrongMonths: [2, 3, 4], seasonMediumMonths: [1, 5, 6, 8] },
 	{imgNameFile: "pessego_nacional", name: "Pêssego Nacional", seasonStrongMonths: [10, 11], seasonMediumMonths: [0, 9] },
 	{imgNameFile: "pessego_estrangeiro", name: "Pêssego Estrangeiro", seasonStrongMonths: [1, 2, 11], seasonMediumMonths: [0, 6, 10] },
 	{imgNameFile: "quincam", name: "Quincam", seasonStrongMonths: [4, 5, 6, 7], seasonMediumMonths: [2, 3, 8, 9] },
 	{imgNameFile: "roma", name: "Romã", seasonStrongMonths: [11], seasonMediumMonths: [0] },
 	{imgNameFile: "seriguela", name: "Seriguela", seasonStrongMonths: [1, 2] },
 	{imgNameFile: "tamarindo", name: "Tamarindo", seasonStrongMonths: [8], seasonMediumMonths: [7, 9] },
 	{imgNameFile: "tangerina_cravo", name: "Tangerina Cravo", seasonStrongMonths: [2, 3], seasonMediumMonths: [4] },
 	{imgNameFile: "tangerina_murcote", name: "Tangerina Murcote", seasonStrongMonths: [7, 8, 9, 10], seasonMediumMonths: [0, 11] },
 	{imgNameFile: "tangerina_poncam", name: "Tangerina Poncam", seasonStrongMonths: [4, 5, 6, 7], seasonMediumMonths: [3] },
 	{imgNameFile: "uva_italia", name: "Uva Itália", seasonStrongMonths: [1, 2, 11], seasonMediumMonths: [0] },
 	{imgNameFile: "uva_niagara", name: "Uva Niagara", seasonStrongMonths: [11], seasonMediumMonths: [0] },
 	{imgNameFile: "uva_rubi", name: "Uva Rubi", seasonStrongMonths: [0, 1, 2, 11], seasonMediumMonths: [3, 4, 5] },
 	{imgNameFile: "uva_estrangeira", name: "Uva Estrangeira", seasonStrongMonths: [2, 3, 4], seasonMediumMonths: [1] }
  ];
}
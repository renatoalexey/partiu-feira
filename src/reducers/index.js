import { combineReducers } from "redux";
import FruitsReducer from "./reducer_fruits";

const rootReducer = combineReducers({
  fruits: FruitsReducer
});

export default rootReducer;
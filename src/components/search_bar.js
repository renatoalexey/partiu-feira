import React, {Component} from 'react';
import TextInput from 'react-native';
import { Container, Header, Item, Input, Icon, Button, Text } from 'native-base';

export default class SearchBar extends Component {
	constructor(props) {
	    super(props);
	    this.state = {term : ''};
	}

	render() {
        return (

            <Header searchBar rounded>
              <Item>
                <Icon name="ios-search" />
                <Input placeholder="Search"
                    text={this.state.term}
                    onChangeText={ term => this.onInputChange(term)} />
                <Icon name="ios-people" />
              </Item>
              <Button transparent>
                <Text>Search</Text>
              </Button>
            </Header>

        );
      }

      onInputChange(term) {
      		this.setState({term});
      		this.props.onSearchTermChange(term);
      	}
}
import React from 'react';

import { seasonality } from "../Constants";
import { Col, Row } from 'react-native-easy-grid';

export default (props) => {
    const numColumns = props.numColumns;
    const fruitSeasonality = props.fruitSeasonality;

    let i = 0;
    let rows = [];
    console.log('#########fruitSeasonality is: ' + fruitSeasonality);
    while(i < fruitSeasonality.length) {
        rows.push( createRow(fruitSeasonality.slice(i, i + numColumns) ));
        i += numColumns;
    }

    return rows;
}

const createRow = (fruitSeasonalityRow) => {
    let columns = [];
    fruitSeasonalityRow.map( (seasonColumn) => {
      columns.push( <Col style={ { backgroundColor: seasonColumn} }> </Col>);
    });

    return <Row> {columns} </Row>;
}
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { List, ListItem, Separator, Container  } from 'native-base';
import { View,Text, ScrollView } from 'react-native';
import { Collapse, CollapseHeader, CollapseBody } from "accordion-collapse-react-native";

import { seasonality, defineSeason, styles, monthsCalendar } from "../Constants";

class SeasonMonths extends Component {

    buildMapMonthFruits() {

        const fruits = this.props.fruits;
        let mapMonthFruits = new Map();

        monthsCalendar.forEach( month => {
            let monthFruits = fruits.filter( fruit => {
                return fruit.seasonStrongMonths.includes(month.number);
            });
            mapMonthFruits.set(month, monthFruits);
        } );

        return mapMonthFruits;
    }

    renderListCollapse(mapMonthFruits) {

        let collapseList = [];
            for(let entry of mapMonthFruits)  {
                const currentMonth = entry[0];
                const monthFruits = entry[1];

                const body = (
                    <Collapse key={currentMonth.number}>
                        <CollapseHeader>
                            <Separator bordered>
                                <Text>{currentMonth.text}</Text>
                            </Separator>
                        </CollapseHeader>
                        {this.renderListItem(currentMonth.number, monthFruits)}
                    </Collapse>
                );

                collapseList.push(body);
            }

        return collapseList;
    }

    renderListItem(numericMonth, monthFruits) {
        let listItemFruits =  monthFruits.map( fruit => {
            const mykey = fruit.name + numericMonth;
            return (
                <ListItem key={mykey}>
                    <Text>
                        {fruit.name}
                    </Text>
                </ListItem>
             );
        } );

        return (
            <CollapseBody>{listItemFruits}</CollapseBody>
        );
    }

    render() {

        const mapMonthFruits = this.buildMapMonthFruits();

        const collapseList = this.renderListCollapse(mapMonthFruits);

        return (<ScrollView>{collapseList.map(listItem => {return (listItem)})}</ScrollView>);
    }
}

function mapStateToProps(state) {
  return {
    fruits: state.fruits
  };
}

export default connect(mapStateToProps)(SeasonMonths);
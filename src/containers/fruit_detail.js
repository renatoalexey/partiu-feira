import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Container, Header, Content, Card, CardItem, Body, Text } from 'native-base';
import { Row, Grid, Col } from 'react-native-easy-grid';
import { View, ScrollView, Image } from 'react-native';

import Rows from "../components/rows";
import { seasonality, defineSeason, styles, images, monthsCalendar } from "../Constants";

export default (props) => {
        const { navigation } = props;

        const fruit = navigation.getParam('fruit', undefined);
        const seasonColorImg = fruitSeasonality(fruit);

    return (
     <Container>
        <Header>
            <Text>{fruit.name}</Text>
        </Header>

        <Grid style={ { width: '100%', marginHorizontal: '0%'} } >
                {renderList(4, seasonColorImg)}
        </Grid>
             <Card style={ { height: '30%' }} >
                    <CardItem>
                      <Body>
                        <Text>
                           Informações sobre a fruta aqui
                        </Text>
                      </Body>
                    </CardItem>
                  </Card>
        </Container>
     );
}

const createColumns = (i, numColumns, seasonColorImg) => {
        let columns = [];
        for(let j = i*numColumns; j < numColumns + i*numColumns; j ++) {
            columns.push(
                <Col key={j} style={{ padding: 0 }}>
                    <View >
                        <Image style={styles.calendarImg} source={seasonColorImg[j].seasonImg} />
                    </View>
                    <Text style={ { textAlign: 'center' } } >{monthsCalendar[j].text}</Text>
                </Col>
            );
        }

        return columns;
}

const renderList = (numColumns, seasonColorImg) => {

     let rows = [];

    for(let i = 0; i < 3; i ++) {
       const columns = createColumns(i, numColumns, seasonColorImg);
       rows.push(<Row key={i} style={ { marginBottom: 15 } }>{columns}</Row>);
    }

    return rows;
}

const fruitSeasonality = (fruit) => {
        return monthsCalendar.map( (month) => {
            return defineSeason(fruit, month.number);
        });
   }

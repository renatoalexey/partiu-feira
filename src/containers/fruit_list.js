import _ from 'lodash';
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Container, Header, Text, Icon ,Left, Body, Right, Title} from 'native-base';
import { Row, Grid } from 'react-native-easy-grid';
import { ScrollView, Button } from 'react-native';

import Rows from "../components/rows";
import SearchBar from "../components/search_bar";
import { seasonality, defineSeason, styles } from "../Constants";

class FruitList extends Component {

    constructor(props) {
        super(props);

        this.state = {
            searchedFruits: props.fruits
        };
    }


    sortFruitsBySeason( fruits ) {
        let sortedFruitsBySeason = [];
        for (const currentSeason of Object.keys(seasonality)) {
            const fruitsCurrentSeason = fruits.filter( fruit => {
                return defineSeason( fruit, new Date().getMonth() ).seasonColor === seasonality[currentSeason];
            });
            sortedFruitsBySeason = sortedFruitsBySeason.concat(fruitsCurrentSeason);
        }
      return sortedFruitsBySeason;
    }

    fruitSearch(term) {
        const fruits = this.props.fruits;

        const searchedFruits = fruits.filter( fruit => {
            return fruit.name.toUpperCase().includes(term.toUpperCase());

        });

        this.setState({searchedFruits});
    }

    renderList() {
        const sortedFruitsBySeasonality = this.sortFruitsBySeason( this.state.searchedFruits );
        return sortedFruitsBySeasonality.map(fruit => {
            const { navigate } = this.props.navigation;
            return (
                <Row key={fruit.name} style={ { backgroundColor: defineSeason(fruit, new Date().getMonth()).seasonColor
                    , height: 30, width: '100%', borderBottomWidth: 1
                        } }  onPress={() => navigate('FruitDetail', { fruit })}>
                    <Text style={ {textAlignVertical: 'center'} } >{fruit.name}</Text>
                    <Icon name="arrow-forward" style={styles.greaterImg}/>
                </Row>
            );
        });
    }

  render() {

    const fruitSearch = _.debounce((term) => {
    			this.fruitSearch(term)
    }, 300);

    const { navigate } = this.props.navigation;
    return (
        <Container>
            <SearchBar onSearchTermChange = {fruitSearch} />
            <Button title="Go to months"
                onPress={() => navigate('SeasonMonths')}/>
            <ScrollView>
                <Grid>
                    {this.renderList()}
                </Grid>
            </ScrollView>
        </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    fruits: state.fruits
  };
}

export default connect(mapStateToProps)(FruitList);
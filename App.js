import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import { Provider, connect } from 'react-redux';
import { createStackNavigator } from 'react-navigation';
import { Root } from "native-base";
import { Font, AppLoading } from "expo";

import reducer from './src/reducers';
import FruitListScreen from './src/containers/fruit_list';
import FruitDetailScreen from './src/containers/fruit_detail';
import SeasonMonthsScreen from './src/containers/season_months';

global.self = global;

const store = createStore(reducer);

const AppNavigator = createStackNavigator({
  FruitList: { screen: FruitListScreen },
  FruitDetail: { screen: FruitDetailScreen },
  SeasonMonths: { screen: SeasonMonthsScreen }
}, {
     headerMode: 'none'
 });

export default class App extends React.Component {

      constructor(props) {
        super(props);
        this.state = { loading: true };
      }

      async componentWillMount() {
        await Font.loadAsync({
          Roboto: require("native-base/Fonts/Roboto.ttf"),
          Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
          Ionicons: require("native-base/Fonts/Ionicons.ttf")
        });
        this.setState({ loading: false });
      }


      render() {
        if (this.state.loading) {
            return (
              <Root>
                <AppLoading />
              </Root>
            );
          }

        return (
          <Provider store={store}>
              <AppNavigator />
          </Provider>
        );
      }
}